University of Dayton

Department of Computer Science

CPS 491 - Spring 2022

Dr. Phu Phung


## Capstone II

# Team Website ReadME
# Build an E-Commerce Website using BigCommerce


# Team Members

1. Emma Haynes	 	<haynese5@udayton.edu>
2. Matthew Mark	 	<markm1@udayton.edu>
3. Shane Merklin 	<merklins1@udayton.edu>
4. Han Le 			<lex004@udayton.edu> 

# Company Mentors

- Summer Nguyen		<summer.nguyen@novobi.com>
	- Novobi LLC
	- DevOps Leader/Project Manager 

## Project Homepage

<https://cps491s22-team9.bitbucket.io>
